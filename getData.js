const Api = require('./src/api/Api');
const fs = require('fs');
require('dotenv').config();

const api = new Api(
	process.env.REACT_APP_PRISMIC_ENDPOINT,
	process.env.REACT_APP_PRISMIC_TOKEN,
);

api.all().then(response => {
	let results = response ? response.results : {};
	fs.writeFile(`${__dirname}/src/api/local.json`, JSON.stringify(results));
});
