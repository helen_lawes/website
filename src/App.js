import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import StyledApp from './App.styles';

import Home from './pages/Home';
import Footer from './components/Footer/Footer';
import Article from './pages/Article';
import NotFound from './pages/NotFound';
import Preview from './pages/Preview';

const App = () => (
	<StyledApp>
		<Router>
			<Switch>
				<Route
					exact
					path="/"
					render={routeProps => <Home {...routeProps} />}
				/>
				<Route
					path="/article/:uid"
					render={routeProps => <Article {...routeProps} />}
				/>
				<Route
					exact
					path="/preview"
					render={routeProps => <Preview {...routeProps} />}
				/>
				<Route component={NotFound} />
			</Switch>
		</Router>
		<Footer />
	</StyledApp>
);

export default App;
