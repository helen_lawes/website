import './utils/polyfils';
import React from 'react';
import ReactDOM from 'react-dom';

import { ParallaxProvider } from 'react-scroll-parallax';
import DocProvider from './utils/doc';

import App from './App';

ReactDOM.render(
	<ParallaxProvider>
		<DocProvider>
			<App />
		</DocProvider>
	</ParallaxProvider>,
	document.getElementById('root'),
);
