import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import { RichText } from 'prismic-reactjs';

import { withDoc, DocPropTypes } from '../utils/doc';

import SectionGroup from './Home.styles';
import Hero from '../components/Hero/Hero';
import Block from '../components/Block/Block';
import Section from '../components/Section/Section';

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			doc: null,
		};
	}

	componentDidMount() {
		this.props.fetch('homepage').then(doc => this.setState({ doc }));
	}

	align(i) {
		return i % 2 === 0 ? 'right' : 'left';
	}

	render() {
		if (this.state.doc) {
			const { data: doc } = this.state.doc;
			return (
				<React.Fragment>
					<Hero home />
					<main>
						<Block large>{RichText.render(doc.introduction)}</Block>
						<SectionGroup>
							{doc.articles.map((article, i) => {
								const {
									data: articleData,
									uid,
								} = article.article;
								const {
									summary,
									title,
									homepage_image,
								} = articleData;
								return (
									<Section
										key={i}
										heading={RichText.asText(title)}
										image={homepage_image.url}
										link={`article/${uid}`}>
										<p>{summary}</p>
									</Section>
								);
							})}
						</SectionGroup>
					</main>
				</React.Fragment>
			);
		}
		return <h1>Loading...</h1>;
	}
}

Home.description = `
	Homepage of the website
`;

Home.propTypes = {
	/** fetch comes from withDoc context */
	fetch: DocPropTypes.fetch,
	/** location comes from react router */
	location: ReactRouterPropTypes.location,
};

export default withDoc(Home);
