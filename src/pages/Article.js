import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import { RichText } from 'prismic-reactjs';
import ago from 's-ago';

import { withDoc, DocPropTypes } from '../utils/doc';
import { linkResolver, htmlSerializer } from '../utils/prismic';

import Hero from '../components/Hero/Hero';
import Block from '../components/Block/Block';
import Link from '../components/Link/Link';
import NotFound from './NotFound';

import { LastPub } from './Article.styles';

class Article extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			doc: null,
			error: true,
		};
	}

	componentDidMount() {
		const uid = this.props.match.params.uid;
		this.props
			.fetch(uid)
			.then(doc => this.setState({ doc }))
			.catch(() => this.setState({ error: true }));
	}

	componentDidUpdate() {
		window.PrismicToolbar.setup(process.env.REACT_APP_PRISMIC_ENDPOINT);
	}

	render() {
		if (this.state.doc) {
			const { data: doc } = this.state.doc;
			return (
				<React.Fragment>
					<Hero background={doc.hero.url} />
					<Block>
						<Link to="/">Home</Link>
						<main>
							<h1>{RichText.asText(doc.title)}</h1>
							{RichText.render(
								doc.content,
								linkResolver,
								htmlSerializer,
							)}
						</main>
					</Block>
				</React.Fragment>
			);
		} else if (this.state.error) return <NotFound />;
		return <h1>Loading...</h1>;
	}
}

Article.description = `
	Display content for an article	
`;

Article.propTypes = {
	/** fetch comes from withDoc context */
	fetch: DocPropTypes.fetch,
	/** match comes from react router */
	match: ReactRouterPropTypes.match,
	/** location comes from react router */
	location: ReactRouterPropTypes.location,
};

export default withDoc(Article);
