import styled from 'styled-components';

const SectionGroup = styled.div`
	background: #333;
	padding: 200px 0 120px;
	position: relative;
	&:before {
		content: '';
		position: absolute;
		width: calc(50% + 200px);
		right: 0;
		top: 120px;
		bottom: 80px;
		background: #eaebf4;
	}
`;

export default SectionGroup;
