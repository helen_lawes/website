import React from 'react';
import Hero from '../components/Hero/Hero';
import Block from '../components/Block/Block';
import Link from '../components/Link/Link';

const NotFound = () => (
	<React.Fragment>
		<Hero />
		<Block>
			<main>
				<h1>Page not found</h1>
				<p>
					The page you are looking for cannot be found, please try
					again from the <Link to="/">homepage</Link>
				</p>
			</main>
		</Block>
	</React.Fragment>
);

export default NotFound;
