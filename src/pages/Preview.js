import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import Cookies from 'js-cookie';
import qs from 'qs';
import Prismic from 'prismic-javascript';

import { withDoc, DocPropTypes } from '../utils/doc';

class Preview extends React.Component {
	componentWillMount() {
		const params = qs.parse(this.props.location.search.slice(1));
		this.props.preview(params.token).then(url => {
			Cookies.set(Prismic.previewCookie, params.token, {
				expires: 1 / 48,
			});
			this.props.history.push(url);
		});
	}

	render() {
		return <h1>Loading preview....</h1>;
	}
}

Preview.description = `
	For previewing unpublished content
`;

Preview.propTypes = {
	/** fetch comes from withDoc context */
	fetch: DocPropTypes.fetch,
	/** preview comes from withDoc context */
	preview: DocPropTypes.preview,
	/** history comes from react router */
	history: ReactRouterPropTypes.history,
	/** location comes from react router */
	location: ReactRouterPropTypes.location,
};

export default withDoc(Preview);
