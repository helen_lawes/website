import styled from 'styled-components';
import { em } from '../utils/styles';

export const LastPub = styled.p`
	font-size: ${em(12)};
`;
