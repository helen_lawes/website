import styled from 'styled-components';
import Link from '../Link/Link';

const StyledButton = styled.a`
	background: #333;
	color: #fff;
	padding: 10px 15px;
	text-decoration: none;
	margin: 20px 0;
	display: inline-block;
	border: none;
	font-size: inherit;
	font-family: inherit;
`;

export default StyledButton;

export const StyledLink = StyledButton.withComponent(Link);
