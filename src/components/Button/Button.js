import React from 'react';
import PropTypes from 'prop-types';

import StyledButton, { StyledLink } from './Button.styles';

const Button = ({ to, ...otherProps }) =>
	to ? (
		<StyledLink {...otherProps} to={to} />
	) : (
		<StyledButton {...otherProps} />
	);

Button.propTypes = {
	to: PropTypes.string,
};

export default Button;
