import React from 'react';
import PropTypes from 'prop-types';
import SyntaxHighlighter from 'react-syntax-highlighter/prism';
import { tomorrow } from 'react-syntax-highlighter/styles/prism';

import StyledCode, { StyledPre } from './Code.styles';

// trim excess whitespace caused by editor indentation
const trim = code => {
	let lines = code.split(/\n/);
	if (!lines[0]) lines.shift();
	let indentation = /^[\s\t]+/.exec(lines[0]);
	if (indentation) {
		return lines.map(line => line.replace(indentation[0], '')).join('\n');
	}
	return code;
};

const Code = ({ children, language }) => (
	<SyntaxHighlighter
		language={language}
		style={tomorrow}
		CodeTag={StyledCode}
		PreTag={StyledPre}>
		{trim(children)}
	</SyntaxHighlighter>
);

Code.defaultProps = {
	language: 'javascript',
};

Code.propTypes = {
	children: PropTypes.node,
	language: PropTypes.string,
};

export default Code;
