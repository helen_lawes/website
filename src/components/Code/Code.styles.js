import styled from 'styled-components';
import { media } from '../../utils/styles';

const StyledCode = styled.code`
	white-space: pre-wrap !important;
`;

export default StyledCode;

export const StyledPre = styled.pre`
	${media.small`
		margin: 0 -40px !important;	
	`};
`;
