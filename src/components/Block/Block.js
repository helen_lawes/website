import PropTypes from 'prop-types';

import BlockStyles from './Block.styles';

const Block = BlockStyles;

Block.displayName = 'Block';

Block.propTypes = {
	children: PropTypes.node,
};

export default Block;
