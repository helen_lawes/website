import styled from 'styled-components';
import { em, rem, media } from '../../utils/styles';

const StyledBlock = styled.div`
	padding: 0 80px;
	max-width: ${rem(1000)};
	margin: 50px auto 100px;
	${props =>
		props.large &&
		`
		font-size: ${em(20)};	
		line-height: ${em(34, 20)};
		margin-top: 100px;
	`} ${media.small`
		padding-left: 40px;
		padding-right: 40px;
	`}

	em {
		font-style: normal;
		background: #333;
		color: #fff;
		padding: 4px 5px;
	}

	img {
		max-width: 100%;
	}
`;

export default StyledBlock;
