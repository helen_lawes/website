import styled from 'styled-components';

import { getColor } from '../../utils/styles';

const StyledAside = styled.aside`
	border-left: 4px solid ${getColor('primary')};
	background: ${getColor('lightGray')};
	padding: 7px 10px;
	margin: 40px 0;
	line-height: 1.7em;
`;

export default StyledAside;
