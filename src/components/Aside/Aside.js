import PropTypes from 'prop-types';

import StyledAside from './Aside.styles';

const Aside = StyledAside;

Aside.displayName = 'Aside';

Aside.propTypes = {
	children: PropTypes.node,
};

export default Aside;
