import React from 'react';
import PropTypes from 'prop-types';

import StyledSection, { SideImage, SideText, Image } from './Section.styles';

const Section = ({ children, image, heading, link }) => (
	<StyledSection>
		<div>
			<SideImage>
				<Image src={image} alt="" />
			</SideImage>
			<SideText heading={heading} link={link}>
				{children}
			</SideText>
		</div>
	</StyledSection>
);

Section.propTypes = {
	children: PropTypes.node,
	image: PropTypes.string,
	heading: PropTypes.string,
	link: PropTypes.string,
};

export default Section;
