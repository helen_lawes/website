import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { media, rem } from '../../utils/styles';
import Link from '../Link/Link';

const backgroundColor = '#eaebf4';

const StyledSection = styled.div`
	position: relative;
	overflow: hidden;
	padding: ${rem(20)} 0;
	> div {
		display: flex;
		margin: 0 auto;
		max-width: ${rem(850)};
		${media.small`
			display: block;
		`};
	}
`;

export default StyledSection;

export const SideImage = styled.div`
	width: 70%;
	${media.small`
		width: 100%;
	`};
`;

export const Image = styled.img`
	max-width: 100%;
`;

const Text = styled.div`
	width: 50%;
	margin: ${rem(80)} 0 ${rem(80)} -20%;
	padding: 0 ${rem(50)};
	display: inline-flex;
	align-items: center;
	position: relative;
	background: ${backgroundColor};
	${media.small`
		width: 100%;
		margin: ${rem(20)};
	`};
`;

const Heading = styled.h2`
	font-family: 'Raleway';
	font-size: 2em;
	text-transform: uppercase;
	line-height: 1.5em;
	margin-top: 0;
	a {
		color: inherit;
	}
`;

export const SideText = ({ children, heading, link }) => (
	<Text>
		<div>
			<Heading>
				<Link to={link}>{heading}</Link>
			</Heading>
			{children}
		</div>
	</Text>
);

SideText.propTypes = {
	children: PropTypes.node,
	heading: PropTypes.string,
	link: PropTypes.string,
};
