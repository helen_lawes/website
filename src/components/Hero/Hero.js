import React from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';

import StyledHero, { Logo, LogoBackground } from './Hero.styles';

import LogoImg from '../../media/logo.svg';
import LogoWhiteImg from '../../media/logo-white.svg';

const Hero = props => (
	<StyledHero {...props}>
		<LogoBackground {...props}>
			{!props.home ? (
				<Link to="/">
					<Logo
						{...props}
						src={LogoWhiteImg}
						alt="Logo for Helen Lawes, Web Developer. Navigates to homepage."
					/>
				</Link>
			) : (
				<Logo
					{...props}
					src={LogoImg}
					alt="Logo for Helen Lawes, Web Developer."
				/>
			)}
		</LogoBackground>
	</StyledHero>
);

Hero.propTypes = {
	home: PropTypes.bool,
};

export default Hero;
