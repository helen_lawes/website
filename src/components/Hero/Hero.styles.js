import styled, { css } from 'styled-components';

import Background from '../../media/rose-phone.jpg';
import { media, getColor, rem } from '../../utils/styles';

const StyledHero = styled.div`
	background: url(${props => props.background || Background}) center no-repeat;
	background-size: cover;
	height: 70vh;
	max-height: ${rem(1300)};
	${props =>
		props.home &&
		css`
			padding: 150px 0;
		`};
`;

export default StyledHero;

export const Logo = styled.img`
	width: 180px;
	max-width: 100%;
	display: block;
	${props =>
		props.home &&
		css`
			margin: 0 auto;
			width: 250px;
			${media.small`
				width: 200px;
			`};
		`};
	${media.small`
		margin: 0 auto;
	`};
`;

export const LogoBackground = styled.div`
	${props =>
		!props.home &&
		css`
			background: ${getColor('primary')};
			margin-left: 80px;
			display: inline-block;
			padding: 40px 0;
			box-shadow: 2px 2px 10px #666666;
			${media.small`
				margin-left: 30px;
			`};
		`};
`;
