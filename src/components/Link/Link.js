import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const Link = props => (
	<RouterLink {...props} onClick={() => window.scrollTo(0, 0)} />
);

export default Link;
