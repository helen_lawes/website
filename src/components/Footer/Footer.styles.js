import styled from 'styled-components';

const StyledFooter = styled.div`
	padding: 40px 0;
	background: #333;
`;

export default StyledFooter;

export const Image = styled.img`
	width: 50px;
	margin: 0 auto;
	display: block;
	opacity: 0.5;
`;
