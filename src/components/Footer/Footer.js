import React from 'react';

import StyledFooter, { Image } from './Footer.styles';

import Flower from '../../media/flower.svg';

const Footer = () => (
	<StyledFooter>
		<Image src={Flower} alt="" />
	</StyledFooter>
);

export default Footer;
