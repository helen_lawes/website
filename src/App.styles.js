import styled, { injectGlobal } from 'styled-components';
import { getColor, em, media, rem } from './utils/styles';

injectGlobal`
	* {
		box-sizing: border-box;
	}

	html {
		padding: 0;
		margin: 0;
		overflow-x: hidden;
	}

	body {
		font: normal 100%/normal 'Open Sans', sans-serif;
		padding: 0;
		margin: 0;
		overflow-x: hidden;
	}

	h1 {
		font-family: 'Raleway';
		font-size: ${em(40)};
		line-height: 1.5em;
		margin-top: ${rem(40)};

		${media.small`
			font-size: ${em(32)};
		`}
	}

	h2 {
		font-size: ${em(26)};
	}

	h3 {
		font-size: ${em(22)}
	}

	h2, h3, h4, h5 {
		line-height: 1.4em;
		margin-top: ${rem(40)};
	}

	p {
		font-size: ${em(17)};
		line-height: ${em(27)};
		margin-top: ${rem(30)};
	}

	a {
		color: ${getColor('primary')};
	}
`;

const StyledApp = styled.div``;

export default StyledApp;
