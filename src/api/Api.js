const Prismic = require('prismic-javascript');

class Api {
	constructor(endPoint = '', token = null, linkResolver) {
		this.endPoint = endPoint;
		this.token = token;
		this.linkResolver = linkResolver;
	}

	_api() {
		return Prismic.api(this.endPoint, { accessToken: this.token });
	}

	fetch(uid, type = 'article', fetchLinks) {
		return new Promise((resolve, reject) => {
			this._api().then(api => {
				api.getByUID(type, uid, fetchLinks).then(response => {
					if (response) {
						resolve(response);
					}
					reject();
				});
			});
		});
	}

	all() {
		const fetchLinks = [
			'article.title',
			'article.summary',
			'article.homepage_image',
		];
		return new Promise((resolve, reject) => {
			this._api().then(api => {
				api.query('', { fetchLinks }).then(response => {
					if (response) {
						resolve(response);
					}
					reject();
				});
			});
		});
	}

	preview(token) {
		return new Promise(resolve => {
			this._api().then(api => {
				api.previewSession(token, this.linkResolver, '/').then(url =>
					resolve(url),
				);
			});
		});
	}
}

module.exports = Api;
