import data from './local.json';

export default class Local {
	constructor() {
		this.data = data;
	}

	fetch(uid, type = 'article') {
		return new Promise((resolve, reject) => {
			let data = this.data.find(object => {
				return object.uid === uid && object.type === type;
			});
			if (data) resolve(data);
			reject();
		});
	}
}
