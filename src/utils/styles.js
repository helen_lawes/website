import { css } from 'styled-components';

const colors = {
	primary: '#aa2330',
	light: '#bfa5a3',
	medium: '#897270',
	dark: '#574240',
	secondary: '#769620',
	main: '#333',
	lightGray: '#efefef',
};

export const getColor = color => colors[color];

const sizes = {
	medium: 1240,
	small: 550,
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
	acc[label] = (...args) => css`
		@media (max-width: ${sizes[label] / 16}em) {
			${css(...args)};
		}
	`;

	return acc;
}, {});

export const em = (px, basePx = 16) => `${px / basePx}em`;

export const rem = (px, basePx = 16) => `${px / basePx}rem`;
