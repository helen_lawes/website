import React from 'react';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import Prismic from 'prismic-javascript';

import Api from '../../api/Api';
import Local from '../../api/Local';

import { Provider } from './DocContext';
import { linkResolver } from '../prismic';

class Doc extends React.Component {
	constructor() {
		super();
		this.api = new Api(
			process.env.REACT_APP_PRISMIC_ENDPOINT,
			process.env.REACT_APP_PRISMIC_TOKEN,
			linkResolver,
		);
		this.local = new Local();
		this.fetch = this.fetch.bind(this);
		this.preview = this.preview.bind(this);
	}

	fetch(uid) {
		const previewRef = Cookies.get(Prismic.previewCookie);
		let args = [uid];
		if (uid === 'homepage') {
			const fetchLinks = [
				'article.title',
				'article.summary',
				'article.homepage_image',
			];
			args = [...args, 'homepage', { fetchLinks }];
		}
		return previewRef ? this.api.fetch(...args) : this.local.fetch(...args);
	}

	preview(token) {
		return this.api.preview(token);
	}

	get contextValues() {
		return {
			fetch: this.fetch,
			preview: this.preview,
		};
	}

	render() {
		const { children } = this.props;
		return <Provider value={this.contextValues}>{children}</Provider>;
	}
}

Doc.description = `
	Doc provider 
`;

Doc.propTypes = {
	children: PropTypes.node,
};

export default Doc;
