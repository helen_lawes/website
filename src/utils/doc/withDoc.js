import React from 'react';
import { Consumer } from './DocContext';

const withDoc = Component => {
	const C = React.forwardRef((props, ref) => (
		<Consumer>
			{context => <Component {...props} ref={ref} {...context} />}
		</Consumer>
	));
	C.displayName = `withDoc(${Component.displayName || Component.name})`;
	return C;
};

export default withDoc;
