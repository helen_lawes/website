import Doc from './Doc';
export { default as withDoc } from './withDoc';
export { default as DocPropTypes } from './PropTypes';
export default Doc;
