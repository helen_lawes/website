import PropTypes from 'prop-types';

const DocPropTypes = {
	fetch: PropTypes.func,
	preview: PropTypes.func,
};

export default DocPropTypes;
