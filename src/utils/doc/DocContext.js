import React from 'react';

const apiContext = React.createContext();

const { Provider, Consumer } = apiContext;

Provider.displayName = 'Api.Provider';
Consumer.displayName = 'Api.Consumer';

export { Provider, Consumer };

export default apiContext;
