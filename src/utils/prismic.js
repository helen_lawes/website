import React from 'react';
import { RichText } from 'prismic-reactjs';
import Code from '../components/Code/Code';

const Elements = RichText.Elements;

const propsWithUniqueKey = function(props, key) {
	return Object.assign(props || {}, { key });
};

export const htmlSerializer = (type, element, content, children, key) => {
	switch (type) {
		case Elements.preformatted:
			return <Code key={key}>{element.text}</Code>;

		case Elements.hyperlink: {
			const targetAttr = element.data.target
				? { target: element.data.target }
				: element.data.link_type === 'Web'
					? { target: '_blank' }
					: {};
			const relAttr = targetAttr.target
				? { rel: 'noopener noreferrer' }
				: {};
			let props = Object.assign(
				{
					href: element.data.url || linkResolver(element.data),
				},
				targetAttr,
				relAttr,
			);
			return React.createElement(
				'a',
				propsWithUniqueKey(props, key),
				children,
			);
		}
		default:
			return null;
	}
};

export const linkResolver = doc => {
	if (doc.type === 'article') return `/article/${doc.uid}`;
	return '/';
};
